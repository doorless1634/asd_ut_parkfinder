defmodule WhiteBreadConfig do
  use WhiteBread.SuiteConfiguration

  suite name:          "FindParkingPlace",
       context:       FindParkingPlaceContext,
        feature_paths: ["features/find_parking_place.feature"]
  
  suite name:          "RegisterUser",
        context:       RegisterUserContext,
    feature_paths: ["features/register_user.feature"] 

  suite name:           "AllocateParkingPlace",
        context:        AllocateParkingPlaceContext,
        feature_paths: ["features/allocate_parking_place.feature"]

  suite name:           "ChangePassword",
        context:        ChangePasswordContext,
        feature_paths: ["features/change_password.feature"]
end
