    defmodule ChangePasswordContext do
        use WhiteBread.Context
        use Hound.Helpers
        alias ParkTartu.{Repo,User}

        @headless_chrome [additional_capabilities: %{
            chromeOptions: %{ "args" => [
                "--user-agent=#{Hound.Browser.user_agent(:chrome)}",
                "--headless",
                "--no-sandbox",
                "--disable-gpu"
            ]}
         }]
        
        feature_starting_state fn  ->
            Application.ensure_all_started(:hound)    
            %{}
        end
        scenario_starting_state fn _state ->
            Hound.start_session(@headless_chrome)
            #Hound.start_session
            Ecto.Adapters.SQL.Sandbox.checkout(ParkTartu.Repo)
            Ecto.Adapters.SQL.Sandbox.mode(ParkTartu.Repo, {:shared, self()})
            %{}
        end
        scenario_finalize fn _status, _state ->
            Ecto.Adapters.SQL.Sandbox.checkin(ParkTartu.Repo)
            Hound.end_session
        end 
        
        given_ ~r/^the user credential$/, 
        fn state, %{table_data: table} ->
            %{username: username, name: name, current_password: current_password, new_password: new_password} = hd table
            changeset = User.changeset(%User{}, %{name: "Jack", username: "jack", password: "password"})
            Repo.insert!(changeset)
            
            {:ok, state |> Map.put(:username, username) |> Map.put(:name, name) |> Map.put(:current_password, current_password) |> Map.put(:new_password, new_password)}
        end
        
        and_ ~r/^I want to change my password$/,
        fn state ->
            {:ok, state}
        end
        
        and_ ~r/^I open Parktu$/, fn state ->
            navigate_to "/#/login"
            {:ok, state}
        end
        
        and_ ~r/^I login Parktu$/, fn state ->
            fill_field({:id, "username"}, state.username)
            fill_field({:id, "password"}, state.current_password)
            element = find_element(:id, "login")
            click(element)    
            {:ok, state}
        end
        
        and_ ~r/^I open my account$/, fn state ->
            element = find_element(:id, "profile-tab")
            click(element)    
            Process.sleep 2000
            {:ok, state}
        end
        
        and_ ~r/^I change my password$/, fn state ->
            fill_field({:id, "name"}, state.name)
            fill_field({:id, "currentpassword"}, state.current_password)
            fill_field({:id, "password"}, state.new_password)
            fill_field({:id, "password_confirmation"}, state.new_password)
            element = find_element(:id, "update")
            click(element)  
            {:ok, state}
        end

        when_ ~r/^I logout Parktu$/, fn state ->
            element = find_element(:id, "logout")
            click(element)    
            {:ok, state}
        end

        then_ ~r/^I should login with my new password and see parking finding page$/, fn state ->
            fill_field({:id, "username"}, state.username)
            fill_field({:id, "password"}, state.new_password)
            element = find_element(:id, "login")
            click(element)    

            assert search_element(:id, "parking_location") != {:error, :no_such_element}
            {:ok, state}
        end
        
    end