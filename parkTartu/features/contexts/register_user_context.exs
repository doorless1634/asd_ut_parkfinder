defmodule RegisterUserContext do
  use WhiteBread.Context
  use Hound.Helpers

  @headless_chrome [additional_capabilities: %{
    chromeOptions: %{ "args" => [
        "--user-agent=#{Hound.Browser.user_agent(:chrome)}",
        "--headless",
        "--no-sandbox",
        "--disable-gpu"
   ]}
  }]
  
  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)    
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session(@headless_chrome)
    #Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(ParkTartu.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(ParkTartu.Repo, {:shared, self()})
    %{}
  end
  scenario_finalize fn _status, _state ->
    Ecto.Adapters.SQL.Sandbox.checkin(ParkTartu.Repo)
    Hound.end_session
  end 

  given_ ~r/^the user credentials$/, 
  fn state, %{table_data: table} ->
    %{username: username, name: name, password: password} = hd table
    {:ok, state |> Map.put(:name,name) |> Map.put(:username, username) |> Map.put(:password, password)}
  end
  
  and_ ~r/^I want to register Parktu$/,
  fn state ->
    {:ok, state}
  end
  
  and_ ~r/^I open Parktu$/, fn state ->
    navigate_to "/#/login"
    {:ok, state}
  end
  
  and_ ~r/^I open registration page$/, fn state ->
    element = find_element(:id, "register")
    click(element)    
    {:ok, state}
  end
  
  when_ ~r/^I fill my information$/, fn state ->
    fill_field({:id, "name"}, state.name)
    fill_field({:id, "username"}, state.username)
    fill_field({:id, "password"}, state.password)
    element = find_element(:id, "register")
    click(element)    
    {:ok, state}
  end
  
  then_ ~r/^I should register and see parking finding page$/, fn state ->
    assert search_element(:id, "parking_location") != {:error, :no_such_element}
    {:ok, state}
  end

end

