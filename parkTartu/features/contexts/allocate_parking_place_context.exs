defmodule AllocateParkingPlaceContext do
    use WhiteBread.Context
    use Hound.Helpers
    alias ParkTartu.{Repo,User,Parking}

    @headless_chrome [additional_capabilities: %{
      chromeOptions: %{ "args" => [
          "--user-agent=#{Hound.Browser.user_agent(:chrome)}",
          "--headless",
          "--no-sandbox",
          "--disable-gpu"
      ]}
    }]
    
    feature_starting_state fn  ->
      Application.ensure_all_started(:hound)    
      %{}
    end
    scenario_starting_state fn _state ->
      Hound.start_session(@headless_chrome)
     # Hound.start_session
      Ecto.Adapters.SQL.Sandbox.checkout(ParkTartu.Repo)
      Ecto.Adapters.SQL.Sandbox.mode(ParkTartu.Repo, {:shared, self()})
      %{}
    end
    scenario_finalize fn _status, _state ->
      Ecto.Adapters.SQL.Sandbox.checkin(ParkTartu.Repo)
      Hound.end_session
    end 
  
    given_ ~r/^the parking places$/, 
    fn state, %{table_data: table} ->

      changeset = User.changeset(%User{}, %{name: "Darkness", username: "darkness", password: "yoo"})
      Repo.insert!(changeset)

      table
      |> Enum.map(fn parking_data -> Parking.changeset(%Parking{}, parking_data) end)
      |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
      
      {:ok, state}
    end
    
    and_ ~r/^I want to find a parking place in "(?<destination>[^"]+)" from "(?<starttime>[^"]+)" to "(?<endtime>[^"]+)"$/,
    fn state, %{destination: destination, starttime: starttime, endtime: endtime} ->
      {:ok, state |> Map.put(:destination, destination) |> Map.put(:starttime, starttime) |> Map.put(:endtime, endtime)}
    end
    
    and_ ~r/^I open Parktu$/, fn state ->
      navigate_to "/#/login"
      {:ok, state}
    end

    and_ ~r/^I login the system$/, fn state ->
      fill_field({:id, "username"}, "darkness")
      fill_field({:id, "password"}, "yoo")
      element = find_element(:id, "login")
      click(element)    
      {:ok, state}
      end
     
    and_ ~r/^I enter my destination, start time and end time$/, fn state ->
      input_into_field({:id, "parking_location"}, state.destination)
      input_into_field({:id, "parking_starting_time"}, state.starttime)
      input_into_field({:id, "parking_end_time"}, state.endtime)
      element = find_element(:id, "submitParking")
      click(element)    
      {:ok, state}
    end
    
    when_ ~r/^I allocate first available parking place$/, fn state ->
        table = find_element(:class,"table")
        element = find_within_element(table,:id,"candidates")
        allocation = find_within_element(element,:id,"1")
        click(allocation)    
        {:ok, state}
    end

    and_ ~r/^I select hourly payment$/, fn state ->
      element = find_element(:id, "two")
      click(element)
      {:ok, state}
    end

    and_ ~r/^I submit my allocation$/, fn state ->
        element = find_element(:id, "submitAllocation")
        click(element)    
        {:ok, state}
      end

    then_ ~r/^I should be informed that allocation is successful$/, fn state ->
        assert search_element(:id, "parking_location") != {:error, :no_such_element}
        {:ok, state}
    end
  
  end