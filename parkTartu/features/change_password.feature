Feature: Change Password of User
  As a customer
  Such that I find my password weak
  I want to change my password

  Scenario: Changing Password of User
    Given the user credential
          | name     | username   | current_password       | new_password |
          | Jack     | jack       | password               | darkness     |
    And I want to change my password
    And I open Parktu
    And I login Parktu
    And I open my account
    And I change my password
    When I logout Parktu
    Then I should login with my new password and see parking finding page