Feature: Allocate Parking Space
  As a customer
  Such that I find list of parking places
  I want to allocate a parking place

  Scenario: Allocating a parking place in Ülikooli 18
    Given the parking places
          | name     | latitude   | longitude  | street   | zone | stopamount |
          | 2        | 58.381694  | 26.719164  | Munga    | A    | 10         |
          | 1        | 58.381756  | 26.722243  | Gildi    | A    | 10         |
          | 1        | 58.381604  | 26.723187  | Kompanii | A    | 10         |
          | 1        | 58.381075  | 26.721985  | Küütri   | A    | 10         |
          | 1        | 58.380681  | 26.719775  | Jakobi   | A    | 10         |
          | 1        | 58.381131  | 26.720451  | Ülikooli | A    | 10         |
    And I want to find a parking place in "Ülikooli 18" from "15:00" to "16:00"
    And I open Parktu
    And I login the system
    And I enter my destination, start time and end time
    When I allocate first available parking place
    And I select hourly payment
    And I submit my allocation
    Then I should be informed that allocation is successful