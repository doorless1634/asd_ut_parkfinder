Feature: Register User
  As a customer
  Such that I go to Tartu
  I want to register to Parktu

  Scenario: Registering a new user Jack
    Given the user credentials
          | name     | username   | password       |
          | Jack     | jack       | password       |
    And I want to register Parktu
    And I open Parktu
    And I open registration page
    When I fill my information
    Then I should register and see parking finding page