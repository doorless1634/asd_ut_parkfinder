Feature: Find Parking Place
  As a customer
  Such that I go to destination
  I want to find a parking place

  Scenario: Finding a parking place in Ülikooli 18
    Given the parking places
          | name     | latitude   | longitude  | street   | zone | stopamount |
          | 2        | 58.381694  | 26.719164  | Munga    | A    | 10         |
          | 1        | 58.381756  | 26.722243  | Gildi    | A    | 10         |
          | 1        | 58.381604  | 26.723187  | Kompanii | A    | 10         |
          | 1        | 58.381075  | 26.721985  | Küütri   | A    | 10         |
          | 1        | 58.380681  | 26.719775  | Jakobi   | A    | 10         |
          | 1        | 58.381131  | 26.720451  | Ülikooli | A    | 10         |
    And I want to find a parking place in "Ülikooli 18" from "13:00" to "14:00"
    And I open Parktu
    And I login the system
    When I enter my destination, start time and end time
    Then I should see some parking places