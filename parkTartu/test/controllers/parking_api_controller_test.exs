defmodule ParkTartu.ParkingAPIControllerTest do
    use ExUnit.Case
    use ParkTartu.ConnCase
    alias ParkTartu.{Parking, Repo, User}

    test "Find parking places from the given destination", %{conn: conn} do
        [%{ latitude: 58.378351, longitude: 26.726916, street: "Uueturu", name: "1", zone: "A", stopamount: 10 },
        %{ latitude: 58.37761, longitude: 26.725384, street: "Uueturu", name: "2", zone: "A", stopamount: 10 },
        %{ latitude: 58.382997, longitude: 26.72159, street: "Magasini", name: "1", zone: "A", stopamount: 10 },
        %{ latitude: 58.382252, longitude: 26.721929, street: "Munga", name: "1", zone: "A", stopamount: 10 },
        %{ latitude: 58.381694, longitude: 26.719164, street: "Munga", name: "2", zone: "A", stopamount: 10 },
        %{ latitude: 58.381756, longitude: 26.722243, street: "Gildi", name: "1", zone: "A", stopamount: 10 }]
        |> Enum.map(fn parking_data -> Parking.changeset(%Parking{}, parking_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

        [%{name: "Jack", username: "jack", password: "password"}] 
        |> Enum.map(fn parking_data -> User.changeset(%User{}, parking_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

        conn2 = post conn, "/api/sessions", %{"username" => "jack", "password" => "password"}
        %{"token" => token} = Poison.decode! conn2.resp_body
        conn = Plug.Conn.put_req_header(conn, "authorization", token)

        conn = post conn, "/api/parkings", %{"lat" => 58.381684, "lng" => 26.719208, "starttime" => 1513364400000, "endtime" => 1513366200000, "distance" => 200}
        %{"parkings" => result} = Poison.decode! conn.resp_body
        assert length(result) > 0
        assert conn.status == 200
    end

    test "No parking place (Destination is outside Tartu)", %{conn: conn} do
        [%{ latitude: 58.378351, longitude: 26.726916, street: "Uueturu", name: "1", zone: "A", stopamount: 10 },
        %{ latitude: 58.37761, longitude: 26.725384, street: "Uueturu", name: "2", zone: "A", stopamount: 10 },
        %{ latitude: 58.382997, longitude: 26.72159, street: "Magasini", name: "1", zone: "A", stopamount: 10 },
        %{ latitude: 58.382252, longitude: 26.721929, street: "Munga", name: "1", zone: "A", stopamount: 10 },
        %{ latitude: 58.381694, longitude: 26.719164, street: "Munga", name: "2", zone: "A", stopamount: 10 },
        %{ latitude: 58.381756, longitude: 26.722243, street: "Gildi", name: "1", zone: "A", stopamount: 10 }]
        |> Enum.map(fn parking_data -> Parking.changeset(%Parking{}, parking_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

        [%{name: "Jack", username: "jack", password: "password"}] 
        |> Enum.map(fn parking_data -> User.changeset(%User{}, parking_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

        conn2 = post conn, "/api/sessions", %{"username" => "jack", "password" => "password"}
        %{"token" => token} = Poison.decode! conn2.resp_body
        conn = Plug.Conn.put_req_header(conn, "authorization", token)

        conn = post conn, "/api/parkings", %{"lat" => 0, "lng" => 0, "starttime" => 1513352880000, "endtime" => 1513352880000, "distance" => 200}
        %{"parkings" => result} = Poison.decode! conn.resp_body
        assert length(result) == 0
        assert conn.status == 200
    end
end