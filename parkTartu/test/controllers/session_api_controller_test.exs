defmodule ParkTartu.SessionAPIControllerTest do
    use ParkTartu.ConnCase
    alias ParkTartu.{Repo, User}

    test "Register a new user", %{conn: conn} do
        conn = post conn, "/api/sessions/register", %{name: "Martin", username: "martin", password: "password"}
        status = conn.status
        %{"msg" => message} = Poison.decode! conn.resp_body
        assert status == 200
        assert message == "User succesfully created"
    end

    test "Not register the existing user", %{conn: conn} do
        [%{name: "Jack", username: "jack", password: "password"}] 
        |> Enum.map(fn parking_data -> User.changeset(%User{}, parking_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

        conn = post conn, "/api/sessions/register", %{name: "Jack", username: "jack", password: "password"}
        status = conn.status
        %{"msg" => message} = Poison.decode! conn.resp_body
        assert status == 200
        assert message == "User already exist"
    end

    test "Login user with correct information", %{conn: conn} do
        [%{name: "Jack", username: "jack", password: "password"}] 
        |> Enum.map(fn parking_data -> User.changeset(%User{}, parking_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

        conn = post conn, "/api/sessions", %{"username" => "jack", "password" => "password"}
        status = conn.status
        %{"result" => result} = Poison.decode! conn.resp_body
        assert status == 201
        assert result == "success"
    end
    
    test "Fail to Login", %{conn: conn} do
        conn = post conn, "/api/sessions", %{"username" => "jack", "password" => "password1"}
        status = conn.status
        %{"result" => result} = Poison.decode! conn.resp_body
        assert status == 400
        assert result == "fail"
    end

    test "Change password of current user", %{conn: conn} do
        [%{name: "Jack", username: "jack", password: "password"}] 
        |> Enum.map(fn parking_data -> User.changeset(%User{}, parking_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

        
        conn2 = post conn, "/api/sessions", %{"username" => "jack", "password" => "password"}
        %{"token" => token} = Poison.decode! conn2.resp_body
        conn = Plug.Conn.put_req_header(conn, "authorization", token)
        conn = post conn, "api/sessions/update", %{"username" => "jack", "currentpassword" => "password", "name" => "Jack", "password" => "darkness"}
        status = conn.status
        %{"msg" => result} = Poison.decode! conn.resp_body
        assert status == 200
        assert result == "User succesfully updated"
    end

    test "Cannot change password of current user", %{conn: conn} do
        [%{name: "Jack", username: "jack", password: "password"}] 
        |> Enum.map(fn parking_data -> User.changeset(%User{}, parking_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

        conn2 = post conn, "/api/sessions", %{"username" => "jack", "password" => "password"}
        %{"token" => token} = Poison.decode! conn2.resp_body
        conn = Plug.Conn.put_req_header(conn, "authorization", token)
        conn = post conn, "api/sessions/update", %{"username" => "jack", "currentpassword" => "password123", "name" => "Jack", "password" => "darkness"}

        status = conn.status
        %{"msg" => result} = Poison.decode! conn.resp_body
        assert status == 200
        assert result == "Current Password is wrong"
    end

    test "Logout", %{conn: conn} do
        [%{name: "Jack", username: "jack", password: "password"}] 
        |> Enum.map(fn parking_data -> User.changeset(%User{}, parking_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

        conn2 = post conn, "/api/sessions", %{"username" => "jack", "password" => "password"}
        %{"token" => token} = Poison.decode! conn2.resp_body
        conn = Plug.Conn.put_req_header(conn, "authorization", token)
        conn = delete conn, "api/sessions/1", %{}

        status = conn.status
        %{"msg" => result} = Poison.decode! conn.resp_body
        assert status == 200
        assert result == "Good bye"
    end
end