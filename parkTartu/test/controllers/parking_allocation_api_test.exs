defmodule ParkTartu.ParkingAllocationAPIControllerTest do
    use ParkTartu.ConnCase
    alias ParkTartu.{Repo,User}

    test "Allocate the given parking space", %{conn: conn} do
        [%{name: "Jack", username: "jack", password: "password"}] 
        |> Enum.map(fn parking_data -> User.changeset(%User{}, parking_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

        conn2 = post conn, "/api/sessions", %{"username" => "jack", "password" => "password"}
        %{"token" => token} = Poison.decode! conn2.resp_body
        conn = Plug.Conn.put_req_header(conn, "authorization", token)
        
        conn = post conn, "/api/parkingallocations", %{"parking_id" => 23, "parking_allocation" => %{"username" => "jack", "starttime" => 1523352100000, "endtime" => 1523352100000}}
        %{"msg" => message} = Poison.decode! conn.resp_body
        %{"result" => result} = Poison.decode! conn.resp_body
        assert message == "your allocation created"
        assert result == "succesfull"
    end
end