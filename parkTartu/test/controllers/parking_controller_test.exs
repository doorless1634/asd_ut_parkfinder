defmodule Takso.ParkingControllerTest do
    use ParkTartu.ConnCase
    alias ParkTartu.{Parking, Repo}
    
    ##### README: It is needed to parse request body and see if there is result but could be checked in BDD.

      test "There are some parking places in the given street", %{conn: conn} do
        [%{name: "Narva", stopamount: 1, zone: "A",street: "Narva mnt 27"},
        %{name: "Kiivi", stopamount: 12, zone: "B",street: "Narva mnt 27"}]
        |> Enum.map(fn parking_data -> Parking.changeset(%Parking{}, parking_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
        conn = post conn, "/parkings/search", %{"parking" => %{"name" => "Narva mnt 27"}}
        #IO.inspect(conn |> Map.get(:resp_body) |> String.replace("\n","") |> String.replace(" ",""))
        #IO.inspect(conn.status)
        #IO.inspect("################################################################")
        #IO.inspect("################################################################")
        assert conn.status == 200
      end

      test "There are no parking places in the given street", %{conn: conn} do
        [%{name: "Narva", stopamount: 1, zone: "A",street: "Narva mnt 27"},
        %{name: "Kiivi", stopamount: 12, zone: "B",street: "Narva mnt 27"}]
        |> Enum.map(fn parking_data -> Parking.changeset(%Parking{}, parking_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
        conn = post conn, "/parkings/search", %{"parking" => %{"name" => "Riia 132"}}
        #IO.inspect(conn |> Map.get(:resp_body) |> String.replace("\n","") |> String.replace(" ",""))
        #IO.inspect(conn.status)
        assert conn.status == 200
      end
    
    end