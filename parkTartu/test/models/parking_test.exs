defmodule ParkTartu.ParkingTest do
  use ParkTartu.ModelCase

  alias ParkTartu.Parking

  @valid_attrs %{position: "some position", street: "some street", type: "some type"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Parking.changeset(%Parking{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Parking.changeset(%Parking{}, @invalid_attrs)
    refute changeset.valid?
  end
end
