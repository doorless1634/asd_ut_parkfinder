defmodule ParkTartu.Repo.Migrations.CreateParkingAllocation do
  use Ecto.Migration
  def change do
    create table(:parkingallocations) do 
      add :starttime, :naive_datetime
      add :endtime, :naive_datetime
      add :parking_id, references(:parkings, on_delete: :nothing)

      timestamps()
   end
  create index(:parkingallocations, [:parking_id])

  end
end
