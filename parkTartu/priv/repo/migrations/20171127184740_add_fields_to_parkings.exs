defmodule ParkTartu.Repo.Migrations.AddFieldsToParkings do
  use Ecto.Migration

  def change do
    alter table(:parkings) do
      add :latitude, :float
      add :longitude, :float
    end
  end
end
