defmodule ParkTartu.Repo.Migrations.AlterParkingAllocationsForUser do
  use Ecto.Migration

  def change do
    alter table(:parkingallocations) do
      add :user_id, references(:users, on_delete: :nothing)
   end
  # create index(:users, [:user_id])

  end
end
