defmodule ParkTartu.Repo.Migrations.CreateParking do
  use Ecto.Migration

  def change do
    create table(:parkings) do
      add :name, :string
      add :street, :string
      add :zone, :string
      add :stopamount, :integer

      timestamps()
    end
  end
end
