defmodule ParkTartu.SessionAPIController do
  use ParkTartu.Web, :controller
  alias ParkTartu.{Repo,User,Authentication}

  def create(conn, %{"username" => username, "password" => password}) do
    user = Repo.get_by(User, username: username)
     
    case Authentication.check_credentials(conn, user, password) do
    {:ok, conn} ->

        {:ok, jwt, _full_claims} = Guardian.encode_and_sign(user, :token)
        conn
        |> json(%{result: "success",token: jwt,name: user.name})
    {:error, conn} ->
        conn
        |> json(%{result: "fail", message: "Bad credentials"})
    end
  end

  def register(conn, userparam) do

    changeset = User.changeset(%User{}, userparam)
        case Repo.insert(changeset) do
          {:ok, _changeset} ->

            user = Repo.all(User, username: Map.get(userparam,"username"))
            if length(user) == 1 do
            theUser = hd user
            {:ok, jwt, _full_claims} = Guardian.encode_and_sign(theUser, :token)
                conn
                |> put_status(200)
                |> json(%{token: jwt,result: "success",msg: "User succesfully created"})
            else
              conn |> json(%{result: "fail",msg: "User already exist"})
            end
          {:error, changeset} ->
            conn |> json(%{result: "fail",msg: "Internal Server Error"})
        end
  end

  def update(conn, %{"username" => username, "currentpassword" => currentpassword,"name"=>name,"password"=>password }) do

    user = Repo.get_by(User, username: username)
    case Authentication.check_credentials(conn, user, currentpassword) do
    {:ok, conn} ->

      changeset = User.changeset(user, %{username: username, name: name, password: password })

          case Repo.update(changeset) do
            {:ok, _changeset} ->
              conn |> json(%{result: "success",msg: "User succesfully updated"})

            {:error, _changeset} ->
              conn |> json(%{result: "fail",msg: "User cannot be updated"})
          end
    {:error, conn} ->
        conn |> json(%{result: "incorrectcurrentpassword", msg: "Current Password is wrong"})
    end
  end

  def delete(conn, _params) do
    {:ok, claims} = Guardian.Plug.claims(conn)
    conn
    |> Guardian.Plug.current_token
    |> Guardian.revoke!(claims)

    conn
    |> put_status(200)
    |> json(%{msg: "Good bye"})
  end

  def unauthenticated(conn, _params) do
    conn
    |> put_status(403)
    |> json(%{msg: "You are not logged in"})
  end
end
