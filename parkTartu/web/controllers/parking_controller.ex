defmodule ParkTartu.ParkingController do
  use ParkTartu.Web, :controller
  alias ParkTartu.{Parking}

  def index(conn, _params) do
    render(conn, "index.html", parkings: [], changeset: Parking.changeset(%Parking{}))
  end
  def show(conn, _params) do
    render(conn, "index.html", parkings: [], changeset: Parking.changeset(%Parking{}))
  end

  def delete(conn, _params) do
    render(conn, "index.html", parkings: [], changeset: Parking.changeset(%Parking{}))
  end
  def edit(conn, _params) do
    render(conn, "index.html", parkings: [], changeset: Parking.changeset(%Parking{}))
  end
  def search(conn, params) do
    #IO.inspect(conn)
    %{"name" => name} = Map.get(params,"parking")

    changeset = Parking.changeset(%Parking{})

    query = from p in Parking,
            where: p.street == ^name,
            select: {p, (fragment("(SELECT COUNT(0) FROM parkingallocations
                                        WHERE parkingallocations.parking_id = ?)", p.id))}
    result = Repo.all(query)

    t= Enum.map(result, fn({x,y}) ->

      %{
        id: xid,
        name: xname,
        stopamount: xstopamount,
        street: xstreet,
        zone: xzone,
      } = x

      %{
        id: xid,
        name: xname,
        stopamount: xstopamount,
        street: xstreet,
        zone: xzone,
        cnt: y
      }

     end)

    render(conn, "index.html", parkings: t , changeset: changeset)
  end
end
