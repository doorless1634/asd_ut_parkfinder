defmodule ParkTartu.ParkingAllocationAPIController do
    use ParkTartu.Web, :controller
    alias ParkTartu.{Parking,ParkingAllocation,User}

   def create(conn, %{"parking_id"=> parking_id, "parking_allocation"=>parking_allocation_params})do

      parking = Repo.get(Parking, parking_id)
      %{"username" => username,"endtime" =>endtime, "starttime"=>starttime} = parking_allocation_params
      user = Repo.get_by(User,username: username)
         changeset =
        parking
        |> Ecto.build_assoc(:parkingallocations)
        |> Ecto.Changeset.change(%{
            starttime: Calecto.DateTimeUTC.cast!(Calendar.DateTime.Parse.js_ms!(to_string(starttime))),
            endtime: Calecto.DateTimeUTC.cast!(Calendar.DateTime.Parse.js_ms!(to_string(endtime)))
        })
        |> Ecto.Changeset.put_assoc(:user,user)

      case Repo.insert(changeset) do
        {:ok ,park} -> 
          ParkTartu.PlaceAllocator.start_link(parking_allocation_params)
          conn |> json(%{result: "succesfull",msg: "your allocation created"})
        {:error, changeset} ->
          conn |> json(%{parkings: "error",msg: "allocation can't be created"})
      end

    end

  end
