defmodule ParkTartu.PageController do
  use ParkTartu.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
