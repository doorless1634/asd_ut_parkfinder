defmodule ParkTartu.ParkingAllocationController do
    use ParkTartu.Web, :controller
    alias ParkTartu.{ParkingAllocation,Parking,Repo}

    def allocate(conn, %{"parking_id"=>parking_id}) do
      changeset = ParkingAllocation.changeset(%ParkingAllocation{})
      render(conn, "add.html", changeset: changeset,parking_id: parking_id)
    end

    def create(conn, %{"parking_id"=> parking_id, "parking_allocation"=>parking_allocation_params})do

      parking = Repo.get(Parking, parking_id)
      changeset =
        parking
        |> Ecto.build_assoc(:parkingallocations)
        |> ParkingAllocation.changeset(parking_allocation_params)

      case Repo.insert(changeset) do
        {:ok ,park} ->
          conn
          |> put_flash(:info, "You allocated successfully.")
          |> redirect(to: parking_path(conn, :index))
      {:error, changeset} ->
        render(conn, "add.html", changeset: changeset,parking_id: parking_id)
      end
    end

end
