defmodule ParkTartu.ParkingAPIController do
  use ParkTartu.Web, :controller
  alias ParkTartu.{Parking}
    def create(conn,   %{"lat"=> lat, "lng" => lng,"starttime"=>starttime,"endtime"=>endtime, "distance" => distance}) do
      IO.puts distance/1000
      starttime_ = Calecto.DateTimeUTC.cast!(Calendar.DateTime.Parse.js_ms!(to_string(starttime)))
      endtime_ = Calecto.DateTimeUTC.cast!(Calendar.DateTime.Parse.js_ms!(to_string(endtime)))

      query1 = from p in Parking,
              select: {p, (fragment("(SELECT COUNT(0) FROM parkingallocations as pa
                                          WHERE pa.parking_id = ?
                                          and (
                                                 (? >= pa.starttime and pa.endtime >= ?)
                                              or (? <= pa.starttime and pa.starttime <= ?)
                                          ))", p.id,^starttime_, ^starttime_,
                                                    ^starttime_, ^endtime_
                                          ))}

      {fromLat, fromLong} = {lat, lng}
      {lat, lng} = degree_to_radian(lat, lng)
      t = Repo.all(query1)
      |> Enum.filter(fn({parking, allocated_count}) ->
        {p_lat_rad, p_lng_rad} = degree_to_radian(parking.latitude, parking.longitude)
        is_in_radius = :math.acos(:math.sin(lat) * :math.sin(p_lat_rad) + :math.cos(lat) * :math.cos(p_lat_rad) * :math.cos(p_lng_rad - (lng))) * 6371 <= distance/1000
        is_space_left = parking.stopamount - allocated_count > 0
        is_in_radius && is_space_left
      end)
      |> Enum.map(fn({parking, accocated_count}) -> %{
          id: parking.id,
          name: parking.name,
          latitude: parking.latitude,
          longitude: parking.longitude,
          remaining: parking.stopamount - accocated_count,
          street: parking.street,
          zone: parking.zone
        }
       end)

       |> Enum.sort_by(fn %{latitude: toLat, longitude: toLong} -> distanceTime(fromLat,fromLong,toLat,toLong) end)
      # render(conn, "index.html", parkings: t , changeset: changeset)
      conn |> json(%{parkings: t})
    end

    def degree_to_radian(lat, lng), do: {lat*:math.pi/180, lng*:math.pi/180}

    def distanceTime(fromLat, fromLong, toLat, toLong) do
      %{body: body} = HTTPoison.get!("http://maps.googleapis.com/maps/api/distancematrix/json?origins=#{fromLat},#{fromLong}&destinations=#{toLat},#{toLong}")
      %{"rows" => list} = Poison.decode!(body)
      ways = Enum.map(list, fn(%{"elements" => [%{"duration"=> x}]}) -> x end)
      min = Enum.map(ways, fn(%{"value"=>value})->value end)
      Enum.min(min)
    end
  end
