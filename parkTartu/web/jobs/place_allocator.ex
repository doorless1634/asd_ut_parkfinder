defmodule ParkTartu.PlaceAllocator do
    use GenServer

    def start_link(request) do
        GenServer.start_link(ParkTartu.PlaceAllocator, request)
    end

    def init(request) do
        currenttimeinmillis =  DateTime.to_unix(DateTime.utc_now())
        %{"username" => username,"endtime" =>endtime, "starttime"=>starttime} =request

        Process.send_after(self(), :notify_customer_created, 500)
        Process.send_after(self(), :notify_customer_start, starttime - (currenttimeinmillis * 1000))
        Process.send_after(self(), :notify_customer_end, endtime - ( currenttimeinmillis * 1000))
        Process.send_after(self(), :notify_customer_in_advance, (endtime - (currenttimeinmillis * 1000) - 10000))

        {:ok, request}
    end
    def handle_info(:notify_customer_created, request) do
       %{"username" => username} =request
        ParkTartu.Endpoint.broadcast("customer:"<>username, "requests", %{msg: "You have successfully reserved your parking slot!"})
        {:noreply, request}
    end

    def handle_info(:notify_customer_start, request) do
        %{"username" => username} =request
        ParkTartu.Endpoint.broadcast("customer:"<>username, "requests", %{msg: "Your allocation started now"})
        {:noreply, request}
    end

    def handle_info(:notify_customer_end, request) do
      %{"username" => username} =request
        ParkTartu.Endpoint.broadcast("customer:"<>username, "requests", %{msg: "Your allocation finished now"})
        {:noreply, request}
    end

    def handle_info(:notify_customer_in_advance, request) do
      %{"username" => username} =request
        ParkTartu.Endpoint.broadcast("customer:"<>username, "requests", %{msg: "Your allocation will finish in 10 seconds"})
        {:noreply, request}
    end
end
