import Vue from "vue";
import VueRouter from "vue-router";
import navbar from "./components/navbar";
import customer from "./components/customer";
import map_parking from "./components/map-parking";
import allocation from "./components/allocation";
import payment from "./components/payment";
import moment from "moment";
import VueMomentJS from "vue-momentjs";
import VeeValidate from 'vee-validate';
import vueSlider from 'vue-slider-component'

import auth from "./components/auth";
import login from "./components/login";
import register from "./components/register";
import account from "./components/account";

Vue.use(VeeValidate);

import main from "./components/main";


import "phoenix";
import "axios";

Vue.use(VueRouter);
Vue.use(VueMomentJS, moment);

Vue.component("navbar", navbar);
Vue.component("customer", customer);
Vue.component("account", account);
Vue.component("allocation", allocation);
Vue.component("map-parking", map_parking);
Vue.component("payment", payment);
Vue.component("login", login);
Vue.component("register", register);
Vue.component("vueSlider", vueSlider);

const requireAuth = (to, _from, next) => {
  if (!auth.authenticated()) {
    next({
      path: '/login',
      query: { redirect: to.fullPath }
    });
  } else {
    next();
  }
}

const afterAuth = (_to, from, next) => {
  if (auth.authenticated()) {
    next(from.path);
  } else {
    next();
  }
}

var router = new VueRouter({
    routes: [
        { path: '/login', component: login, beforeEnter: afterAuth,name: "login" },
        { path: '/register', component: register,name: "register"},
        { path: '/', component: main,beforeEnter: requireAuth  },
        { path: '*', redirect: '/login' },
        { path: '/allocation/:parking_id', name: "allocation",component: allocation,beforeEnter: requireAuth  },
        { path: '/account', name: "account",component: account, beforeEnter: requireAuth  }
    ]
});
new Vue({
    router
}).$mount("#parktu-app");
