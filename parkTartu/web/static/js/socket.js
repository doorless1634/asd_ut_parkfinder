// NOTE: The contents of this file will only be executed if

import {Socket} from "phoenix"
import { EventBus } from './event-bus.js';
import { SocketReconnectBus } from './socket_reconnect.js';

let usertok = window.localStorage.getItem('token-'+window.localStorage.getItem('parktartu_username'));
let socket = new Socket("/socket", {params: {token: usertok}})

function connectandsetup(){
  let usertok = window.localStorage.getItem('token-'+window.localStorage.getItem('parktartu_username'));
  socket = new Socket("/socket", {params: {token: usertok}})
  socket.connect()

  let channel = socket.channel("customer:".concat(window.localStorage.parktartu_username), {});
  channel.join()
      .receive("ok", resp => { console.log("Joined successfully", resp) })
      .receive("error", resp => { console.log("Unable to join", resp) });
  channel.on("requests", payload => {
          EventBus.$emit('allocationevent', payload.msg);
     });
  console.log(socket);
}
    connectandsetup()
SocketReconnectBus.$on('connect_event', msg => {
    connectandsetup()
});
export default socket
