defmodule ParkTartu.ParkingAllocation do
  use ParkTartu.Web, :model
  use Calecto.Schema, usec: true

  schema "parkingallocations" do
    field :starttime, Calecto.DateTimeUTC
    field :endtime, Calecto.DateTimeUTC
    belongs_to :parking, ParkTartu.Parking, foreign_key: :parking_id
    belongs_to :user, ParkTartu.User, foreign_key: :user_id

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:user, :starttime, :endtime])
    |> validate_required([:user, :starttime, :endtime])
  end

end
