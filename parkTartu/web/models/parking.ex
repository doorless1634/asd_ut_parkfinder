defmodule ParkTartu.Parking do
  use ParkTartu.Web, :model

  schema "parkings" do
    field :name, :string
    field :latitude, :float
    field :longitude, :float
    field :street, :string
    field :zone, :string
    field :stopamount, :integer
    
    has_many :parkingallocations, ParkTartu.ParkingAllocation

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :latitude, :longitude, :street, :zone, :stopamount])
    |> validate_required([:name, :latitude, :longitude, :street, :zone, :stopamount])
  end
end
