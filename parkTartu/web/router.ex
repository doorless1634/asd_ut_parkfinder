defmodule ParkTartu.Router do
  use ParkTartu.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :auth_api do
     plug Guardian.Plug.EnsureAuthenticated, handler: ParkTartu.SessionAPIController
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug Guardian.Plug.VerifyHeader
    plug Guardian.Plug.LoadResource
  end

  scope "/", ParkTartu do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index

    resources "/parkings", ParkingController do
        resources "/parkingallocations", ParkingAllocationController ,only:  [:create]
        get "/parkingallocations/allocate", ParkingAllocationController, :allocate

    end

    get "/parkings", ParkingController, :index
    post "/parkings/search/", ParkingController, :search
  end

  scope "/api", ParkTartu do
    pipe_through :api
    post "/sessions", SessionAPIController, :create

    post "/sessions/register", SessionAPIController, :register
  end

  scope "/api", ParkTartu do
    pipe_through [:api, :auth_api]

    delete "/sessions/:id", SessionAPIController, :delete
    post "/sessions/update", SessionAPIController, :update
    post "/parkings", ParkingAPIController, :create
    post "/parkingallocations/", ParkingAllocationAPIController, :create
  end

end
